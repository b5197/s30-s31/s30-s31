
//[SECTION] Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");

//[SECTION] Server Setup
	//establish a connection
	const app = express();
	//define a path/address
	const port = 4000;

//[SECTION] Database Connection
	//connect to MongoDB Atlas
	mongoose.connect('mongodb+srv://kjcaguicla:admin123@cluster0.7nzbd.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',{
		useNewUrlParser:true,
		useUnifiedTopology:true
	});
	//get the credentials of your DB atlas user

	let db = mongoose.connection;
	db.on('error',console.error.bind(console,"Connection Error"));
	db.once('open',()=>console.log("Connected to MongoDB"));

//Middleware
app.use(express.json());

//Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: String
});

//Mongoose Model
const Task = mongoose.model("task", taskSchema);

//Post route to create new task
app.post('/tasks', (req,res) => {
	// console.log(req.body)
	let newTask = new Task ({
		name: req.body.name,
		status: req.body.status
	});
	newTask.save()
	.then(result => res.send({message: "Document creation successful"}))
	.catch(error => res.send({message: "Error in document creation"}))
	// newTask.save((error, savedTask) => {
	// 	if(error){
	// 		res.send("Document creation failed");
	// 	} else {
	// 		res.send(savedTask);
	// 	};
	// });
});

//GET Method
app.get('/tasks', (req, res) => {
	Task.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
});

const sampleSchema = new mongoose.Schema({
	name: String,
	isActive: Boolean
});

const Sample = mongoose.model("samples", sampleSchema);

app.post('/samples', (req,res) => {
	let newSample = new Sample ({
		name: req.body.name,
		isActive: req.body.isActive
	});
	newSample.save((error,savedSample) => {
		if (error) {
			res.send(error);
		} else {
			res.send(savedSample);
		};
	});
});

app.get('/samples', (req, res) => {
	Sample.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
})

// const manggagamitSchema = new mongoose.Schema({
// 	username: String,
// 	isAdmin: Boolean
// });

// const Manggagamit = mongoose.model("manggagamit", manggagamitSchema);

// app.post('/mgaManggagamit', (req, res) => {
// 	let newManggagamit = new Manggagamit ({
// 		name: req.body.username,
// 		isAdmin: req.body.isAdmin
// 	});
// 	newManggagamit.save((savedManggagamit, error) => {
// 		if (error) {
// 			res.send(error);
// 		} else {
// 			res.send(savedManggagamit);
// 		};
// 	});
// });

const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

const user = mongoose.model("user", userSchema);

app.post('/users', (req,res) => {
	let newUser = new user ({
		username: req.body.username,
		password: req.body.password
	});
	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
});

app.get('/users', (req, res) => {
	user.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
})

//[SECTION] Entry Point Response
	//binding the connection to the designated port
	app.listen(port, () => console.log(`Server running at port: ${port}`));
