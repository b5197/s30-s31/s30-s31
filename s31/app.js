//[SECTION] DEPENDENCIES AND MODULES
	const express = require("express");
	const mongoose = require("mongoose");
	const taskRoute = require("./routes/taskRoute");

//[SECTION] SERVER SETUP
	const app = express();
	const port = 4000;

//[SECTION] DB CONNECTION
	mongoose.connect('mongodb+srv://kjcaguicla:admin123@cluster0.7nzbd.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',{
		useNewUrlParser:true,
		useUnifiedTopology:true
	});
	let db = mongoose.connection;
	db.on('error',console.error.bind(console,"Connection Error"));
	db.once('open',()=>console.log("Connected to MongoDB"));

//[SECTION] MIDDLEWARE
	app.use(express.json());
	app.use(express.urlencoded({extended: true}));

//[SECTION] ROUTING SYSTEM
	app.use("/tasks", taskRoute);

//[SECTION] ENTRY POINT
	app.listen(port, () => console.log(`Server running at port: ${port}`));