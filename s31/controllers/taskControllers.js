const Task = require("../models/task");


//[SECTION] CONTROLLERS

//[SECTION] CREATE
	module.exports.createTask = (reqBody) => {
		let newTask = new Task({
			name: reqBody.name
		})
		return newTask.save().then((task, error) => {
			if (error) {
				return false
			} else {
				return task
			};
		});
	};

//[SECTION] RETRIEVE
	module.exports.getAllTasks = () => {
		return Task.find({})
		.then(result => {
			return result
		});
	};

//[SECTION] UPDATE
	module.exports.taskCompleted = (taskId) => {
		return Task.findById(taskId)
		.then((found, error) => {
			if (found) {
				console.log(found);
				found.status = 'Completed';
				return found.save().then((updatedTask, saveErr) => {
					if (updatedTask) {
						return 'Task has been successfully modified'
					} else {
						return 'Task failed to Update'
					}
				})
			} else {
				console.log(error)
				return "Error! No match found";
			};
		});
	};

	module.exports.taskPending = (userInput) => {
		return Task.findById(userInput).then((result, err) => {
			if (result) {
				result.status = 'Pending';
				return result.save().then((taskUpdated, saveErr) => {
					if (taskUpdated) {
						return `Task ${taskUpdated.name} was updated to Pending`
					} else {
						return `Task failed to Update`;
					}
				})
			} else {
				return `Something went wrong`;
			};
		});
	};

//[SECTION] DESTROY
	module.exports.deleteTask = (taskId) => {
		return Task.findByIdAndRemove(taskId)
		.then((fulfilled, rejected) => {
			if (fulfilled) {
				return 'The task has been successfully removed'
			} else {
				return 'Failed to remove Task'
			};

		});
	};