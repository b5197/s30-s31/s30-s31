const express = require("express");

const router = express.Router()

const taskController = require("../controllers/taskControllers")

router.get('/', (req, res) => {
	taskController.getAllTasks()
	.then(resultFromController => res.send(resultFromController))
});

router.post('/', (req,res) => {
	taskController.createTask(req.body)
	.then(resultFromController => res.send(resultFromController))
})

router.delete('/:task', (req,res) => {
	console.log(req.params.task)
	let taskId = req.params.task
	// res.send('Delete Route')
	taskController.deleteTask(taskId)
	.then(resultFromController => res.send(resultFromController))
});

router.put('/:task', (req,res) => {
	console.log(req.params.task)
	let idNiTask = req.params.task
	taskController.taskCompleted(idNiTask)
	.then(resultFromController => res.send(resultFromController))
});

router.put('/:task/pending', (req,res) => {
	let id = req.params.task;
	// console.log(id)
	taskController.taskPending(id)
	.then(resultFromController => res.send(resultFromController))
});






module.exports = router;